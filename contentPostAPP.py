import webapp
import requests

class ContentPostAPP(webapp.Webapp):

    resources = {"/": "P&aacute;gina principal"}

    def getResourcesList(self):
        resources_list = ""
        for key in self.resources.keys():
            resources_list += f"{key} <br>"
        return resources_list

    def parse(self, received):
        received = received.decode('utf8')
        method = received.split(" ")[0]
        resource = requests.utils.unquote(received.split(" ")[1])
        if method == "POST":
            body = "/" + requests.utils.unquote(received.split("\r\n\r\nsubmitted-name=")[1])
        else:
            body = ''
        return method, resource, body

    def process(self, analyzed):
        method, resource, body = analyzed

        if method == "POST":
            self.resources[body] = f"P&aacute;gina de {body}"
        if resource in self.resources.keys():
            http = '200 OK'
            html = '<html><body><h1> ' \
                   + self.resources[resource] +'.<br> Tu petici&oacute;n est&aacute; guardada y es: ' + resource + \
                   '</h1>'
        else:
            http = '200 OK'
            html = "<html><body><h1>" + \
                f"Tu petici&oacute;n: {resource} no se encuentra guardada en el diccionario." \
                "</h1>"
        resources_list = self.getResourcesList()
        html += '<form method="post">'\
                    '<label>Introducir recurso a la lista:' \
                        '<input name="submitted-name" autocomplete="name"/>' \
                    '</label>' \
                    '<button>Guardar</button>'\
                '</form>' \
                '<h1>Lista de recursos guardados:</h1><br>' \
                + resources_list + \
                '</body></html>'
        return http, html


if __name__ == "__main__":
    content = ContentPostAPP('localhost', 1234)