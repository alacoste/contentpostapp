import socket


class Webapp:

    def parse(self, received):
        received = received.decode()
        return received.split(" ")[1]

    def process(self, analyzed):
        http = '200 OK'
        html = '<html><body><h1> Hello World! Tu petici&oacute;n es ' + analyzed + '</h1></body></html>'
        return http, html

    def __init__(self, ip, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((ip, port))
        mySocket.listen(5)
        exit = False
        while not exit:
            try:
                print("Waiting for connections")
                print(f"Listening at port {port}")
                (recvSocket, address) = mySocket.accept()
                print(f"Puerto cliente: {address[1]}")
                print("HTTP request received:")
                received = recvSocket.recv(2048)
                print(received)
                if received:
                    petition = self.parse(received)
                    http, html = self.process(petition)
                    response = 'HTTP/1.1 ' + http + '\r\n\r\n' + html + '\r\n'
                    recvSocket.send(response.encode('utf-8'))
                recvSocket.close()
            except KeyboardInterrupt:
                exit = True


if __name__ == "__main__":
    webapp = Webapp('localhost', 1235)